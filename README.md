# CST2550 Coursework 2

A library management terminal application. Makes use of efficient data structures, with custom implementations (like the hashtable in `include/map.h` or the resizable array in `include/vec.h`). Requires a C++17 compliant compiler (mainly because of `<optional>`).

```shell
# Compile and run the program
make
make run

# Build the program executable
make all

# Compile the test suite
make test

# Compile and run the test suite
make test-run
```
