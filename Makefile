CXX=g++
CXXFLAGS=-Wall -Wextra -Wpedantic -Iinclude -std=c++17

EXE=library
SRCS=$(wildcard src/*.cpp)
OBJS=$(patsubst src/%.cpp, build/tmp/%.o, $(SRCS))
TESTS=$(wildcard test/*.cpp)
TEST_OBJS=$(patsubst test/%.cpp, build/tmp/test/%.o, $(TESTS))

run: all
	./build/bin/$(EXE)

all: build/bin/$(EXE)

build/bin/$(EXE): $(OBJS)
	@mkdir -p build/bin
	$(CXX) $(OBJS) -o build/bin/$(EXE)

build/tmp/%.o: src/%.cpp
	@mkdir -p build/tmp
	$(CXX) $(CXXFLAGS) -c $< -o $@

test: all build/bin/tests

test-run: test
	./build/bin/tests

build/bin/tests: $(TEST_OBJS)
	$(CXX) $(filter-out build/tmp/main.o,$(OBJS)) $(TEST_OBJS) -o build/bin/tests

build/tmp/test/%.o: test/%.cpp
	@mkdir -p build/tmp/test
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	rm -rf build

.PHONY: all run clean test test-run