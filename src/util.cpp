#include "util.h"

/*
 * util.cpp
 * Author: M00697094
 * Created: 14/1/2021
 * Last Updated: 10/4/2021
 */

#include <algorithm>
#include <cctype>
#include <cerrno>
#include <cstring>
#include <fstream>
#include <iostream>
#include <optional>

#include "vec.h"

void clear() {
    // clear the using system commands (cross-platform)
#if defined(WIN32) || defined(_WIN32) || \
    defined(__WIN32) && !defined(__CYGWIN__)
    system("cls");
#else
    system("clear");
#endif
}

/**
 * @brief Safely opens a file throwing a std::runtime_error with a useful
 * message if the file could not be opened.
 *
 * @param path The path of the file you would like to open.
 * @return std::ofstream A file stream to which you write data.
 */
std::fstream open_file(const std::string& path) {
    std::fstream file(path);

    if (!file) {
        std::string error_message = std::strerror(errno);
        println(
            "[Error] File ('{}') could not be opened while attempting to "
            "read.\nError code [{}]: {}.\n",
            path, errno, error_message);

        throw std::runtime_error("Error while reading file.");
    }

    return file;
}

/**
 * @brief Converts a string to lowercase.
 *
 * @param str String to be converted.
 * @return std::string Lowercase version of string.
 */
std::string lowercase(std::string str) {
    std::transform(str.begin(), str.end(), str.begin(),
                   [](unsigned char c) { return std::tolower(c); });
    return str;
}

void kmp_preprocess(const std::string& pattern, vec<size_t>& suffix_array) {
    size_t length = 0;
    suffix_array[0] = 0;
    size_t pattern_length = pattern.size();

    size_t i = 1;
    while (i < pattern_length) {
        if (pattern[i] == pattern[length]) {
            length++;
            suffix_array[i] = length;
            i++;
        } else {
            if (length != 0) {
                length = suffix_array[length - 1];
            } else {
                suffix_array[i] = 0;
                i++;
            }
        }
    }
}

std::optional<size_t> kmp_search(const std::string& pattern,
                                 const std::string& text) {
    size_t text_length = text.size();
    size_t pattern_length = pattern.size();

    if (pattern.empty() || pattern_length > text_length) return std::nullopt;

    vec<size_t> suffix_array;
    suffix_array.reserve(pattern_length);
    kmp_preprocess(pattern, suffix_array);

    size_t i = 0;
    size_t j = 0;

    while (i < text_length) {
        if (pattern[j] == text[i]) {
            j++;
            i++;
        }

        if (j == pattern_length) {
            return i - j;
        } else if (i < text_length && pattern[j] != text[i]) {
            if (j != 0) {
                j = suffix_array[j - 1];
            } else {
                i++;
            }
        }
    }

    return std::nullopt;
}
