/*
 * book.cpp
 * Author: M00697094
 * Created: 26/3/2021
 * Updated: 4/4/2021
 */

#include "book.h"

#include <sstream>

/**
 * @brief Construct an empty Book object. All properties are
 * initialized to sensible default values.
 */
Book::Book() : title(""), authors(""), ISBN(""), quantity(0) {}

/**
 * @brief Constructs a new Book object.
 *
 * @param title The name/title of the book.
 * @param authors The authors, with ';' as delimiter for multiple authors.
 * @param ISBN The standardized unique identifier for the book (without dashes).
 * @param quantity The number of books available to  be sold.
 */
Book::Book(const std::string& title, const std::string& authors,
           const std::string& ISBN, int quantity)
    : title(title), authors(authors), ISBN(ISBN), quantity(quantity) {}

/**
 * @brief Returns a TSV (Tab-Seperated Value) string of this Book object.
 * @return std::string The TSV string (order is: title,authors,ISBN,quantity).
 */
std::string Book::to_tsv() {
    std::ostringstream buffer;
    buffer << title << '\t' << authors << '\t' << ISBN << '\t' << quantity
           << '\t' << std::endl;
    return buffer.str();
}