#include "program.h"

/*
 * program.cpp
 * Author: M00697094
 * Created: 10/4/2021
 * Updated: 10/4/2021
 */

#include "book.h"
#include "map.h"
#include "vec.h"

Book parse_tsv_line(const std::string& string) {
    int i = 0, prev = 0, column_counter = 0, line_length = string.size();

    std::string title, authors, ISBN;
    int quantity;

    while (i < line_length) {
        if (string[i] == '\t') {
            std::string column = string.substr(prev, i - prev);

            if (column_counter == 0) {
                title = column;
            } else if (column_counter == 1) {
                authors = column;
            } else if (column_counter == 2) {
                ISBN = column;
            } else if (column_counter == 3) {
                quantity = std::stoi(column);
            } else {
                throw std::runtime_error(
                    "[TSV Parse Error] Too many columns in a line.");
            }

            column_counter++;
            prev = i + 1;
        }

        i++;
    }

    if (column_counter < 4) {
        throw std::runtime_error(
            "[TSV Parse Error] Too few columns in a line.");
    }

    return Book(title, authors, ISBN, quantity);
}

void parse_tsv(std::istream& stream, map<std::string, Book>& store) {
    Book tmp_book;
    std::string buffer;

    while (std::getline(stream, buffer)) {
        tmp_book = parse_tsv_line(buffer);
        store.insert(tmp_book.title, tmp_book);
    }
}

void save_books_to_file(map<std::string, Book>& store,
                        const std::string& path) {
    std::fstream output_stream = open_file(path);
    for (const std::string& title : store.keys()) {
        output_stream << store.get(title).to_tsv();
    }
    output_stream.close();
}

/* Program Functions */
void add_book(map<std::string, Book>& store, const std::string& path) {
    println("\nPlease input the details for your new book.");
    std::string title = prompt<std::string>(
        "Title: ",
        [&store](const std::string& provided_title) {
            // don't allow the user to overwrite an existing book by-mistake
            return !store.contains(provided_title);
        },
        "[Warning] A book with that title already exists in the store. "
        "Please try a new one.\n\n:: ",
        false);
    std::string authors = prompt<std::string>(
        "Authors (if more than one add \"; \" between the names): ");
    std::string ISBN = prompt<std::string>("ISBN: ");
    int quantity =
        prompt<int>("Quantity: ", [](int input_qty) { return input_qty > 0; });

    store.insert(title, Book(title, authors, ISBN, quantity));
    save_books_to_file(store, path);

    clear();
    println("[Success] Book ('{}') was added to the store.\n", title);
}

std::optional<Book> find_book(map<std::string, Book>& store) {
    int choice = prompt<int>(
        R"(
Please select the type of search you would prefer:

  1. Exact title lookup (faster)
  2. Partial string-based search (slower)

:: )",
        [](int chosen) { return chosen > 0 && chosen < 3; },
        "\n[Warning] Invalid input, please make sure that the entered number "
        "is either `1` or `2`.\n\n:: ",
        false);

    std::string search_string =
        prompt<std::string>("\nPlease enter your search string: ");

    if (choice == 1) {
        return store.lookup(search_string);
    } else if (choice == 2) {
        search_string = lowercase(search_string);

        /*
            slower as it is guaranteed to be O(n) where n = number of books
            it uses a basic algorithm where we go through every book & performs
            a KMP string match using the search_string & title. pseudocode:

                results = []

                for title, book in store {
                    if kmp_search(search_string, title) {
                        results.append(book)
                    }
                }

                // prompt the user to select one of the results
        */

        vec<Book> search_results;

        for (const std::string& title : store.keys()) {
            if (kmp_search(search_string, lowercase(title))) {
                search_results.push_back(store.get(title));
            }
        }

        size_t number_of_results = search_results.size();

        println("Found {} results for your search:\n", number_of_results);

        if (number_of_results == 0)
            return std::nullopt;
        else if (number_of_results == 1) {
            println("    {}", search_results[0].title);
            return search_results[0];
        }

        for (size_t i = 0; i < number_of_results; i++) {
            println("Search Index: {}    Title: {}", i,
                    search_results[i].title);
        }

        int selected_index = prompt<int>(
            "Please select one of the found books (enter search index): ",
            [number_of_results](int selected) {
                return selected >= 0 && (size_t)selected < number_of_results;
            },
            "\n[Warning] Invalid index, please make sure you select an index "
            "from the provided results\n\n:: ",
            false);

        return search_results[selected_index];
    }

    return std::nullopt;
}

void delete_book(map<std::string, Book>& store, const std::string& path) {
    std::optional<Book> search_result;

    println("\nPlease find the book you would like to delete.");

    search_result = find_book(store);
    while (!search_result.has_value()) {
        println("\nPlease try again to find the book to delete.");
        search_result = find_book(store);
    }

    Book to_delete = search_result.value();
    store.remove(to_delete.title);
    save_books_to_file(store, path);

    clear();
    println("[Success] Book ('{}') was deleted from the store.\n",
            to_delete.title);
}

void update_book(map<std::string, Book>& store, const std::string& path) {
    std::optional<Book> search_result;

    println("\nPlease find the book you would like to update.");

    search_result = find_book(store);
    while (!search_result.has_value()) {
        println("\nPlease try again to find the book to update.");
        search_result = find_book(store);
    }

    Book to_update = search_result.value();

    int field_to_update = prompt<int>(
        R"(
Which field would you like to update?

  1. Title
  2. Authors
  3. ISBN
  4. Quantity 

:: )",
        [](int chosen) { return chosen <= 4 && chosen > 0; },
        "\n[Warning] Invalid input, please make sure that the entered number "
        "is an available option.\n\n:: ",
        false);

    if (field_to_update == 1) {
        std::string new_title = prompt<std::string>(
            "\nNew title: ",
            [&store](const std::string& input_title) {
                return !store.contains(input_title);
            },
            "[Warning] A book with that title already exists in the store. "
            "Please try a new one.\n\n:: ",
            false);
        store.remove(to_update.title);
        to_update.title = new_title;
        store.insert(new_title, to_update);
    } else if (field_to_update == 2) {
        std::string new_authors = prompt<std::string>(
            "\nNew authors (delimit with \"; \" for multiple): ");
        to_update.authors = new_authors;
        store.insert(to_update.title, to_update);
    } else if (field_to_update == 3) {
        std::string new_isbn = prompt<std::string>("\nNew ISBN: ");
        to_update.ISBN = new_isbn;
        store.insert(to_update.title, to_update);
    } else if (field_to_update == 4) {
        int new_quantity = prompt<int>("\nNew quantity: ",
                                       [](int new_qty) { return new_qty > 0; });
        if (new_quantity == 0) store.remove(to_update.title);
        to_update.quantity = new_quantity;
        store.insert(to_update.title, to_update);
    }
    save_books_to_file(store, path);

    clear();
    println("[Success] Book ('{}') has been updated.\n", to_update.title);
}

int menu(map<std::string, Book>& store, const std::string& path) {
    int choice = prompt<int>(
        R"(
Please choose an option (0 to quit):

  1. Add a new book
  2. Delete an existing book (requires search)
  3. Search for a book by title
  4. Update book (requires search) 

:: )",
        [](int chosen) { return chosen <= 4 && chosen >= 0; },
        "\n[Warning] Invalid input, please make sure that the entered "
        "number is an available option.\n\n:: ",
        false);

    if (choice == 0) {
        println("\nThank you for using this system!");
        return 0;
    } else if (choice == 1) {
        clear();
        add_book(store, path);
        menu(store, path);
    } else if (choice == 2) {
        clear();
        delete_book(store, path);
        menu(store, path);
    } else if (choice == 3) {
        clear();
        std::optional<Book> result = find_book(store);
        if (result.has_value()) {
            Book found_result = result.value();

            std::istringstream author_buffer(found_result.authors);
            std::string temp_author, authors_by_line;

            while (std::getline(author_buffer, temp_author, ';')) {
                authors_by_line +=
                    "\n        " +
                    /*
                        trim the leading space as the authors
                        are "; " delimited not just ";"
                    */
                    (temp_author[0] == ' '
                         ? temp_author.substr(1, temp_author.size() - 1)
                         : temp_author);
            }

            println(
                "\nYour book was found:"
                "\n    Title: {}\n    Authors: {}"
                "\n    ISBN: {}\n    Quantity: {}\n",
                found_result.title, authors_by_line, found_result.ISBN,
                found_result.quantity);
        } else {
            println("That book could not be found.");
        }

        menu(store, path);
    } else if (choice == 4) {
        clear();
        update_book(store, path);
        menu(store, path);
    }

    return 0;
}
