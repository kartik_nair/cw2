/*
 * main.cpp
 * Author: M00697094
 * Created: 26/3/2021
 * Updated: 4/4/2021
 */

#include <fstream>

#include "book.h"
#include "map.h"
#include "program.h"
#include "util.h"

int main(int argc, char** argv) {
    if ((argc != 1 && argc != 2) ||
        (argc > 1 && std::string(argv[1]) == "--help")) {
        println(
            R"(
USAGE:
    {} [file]

ARGUMENTS:
    file: Optional path to a file that can be loaded as
          the initial store (default: './books.txt').
)",
            argv[0]);
        exit(1);
    }

    std::string path = "books.txt";
    if (argc == 2) path = argv[1];

    map<std::string, Book> store;
    std::fstream input_file = open_file(path);
    parse_tsv(input_file, store);

    println("[Success] Loaded file '{}' into store.", path);

    /*

    Functions for users:

        1. Add a new book
        2. Delete an existing book (requires search)
        3. Search for a book by title
        4. Update book (requires search)

    The entire progam can be thought of as a Finite State Machine, where every
    function represents a single state. Here is a diagram representing all the
    states that the program can be in:

        +---------------------------+
        |                           |
        |                        +--v-----------+
        |      +----------------->              <----+
        |      |                 |     Menu     |    |
        |      |                 |              |    |      +----------------+
        |      |                 +-----------^--+    |      |                |
        |      |                             |       +------>    Add Book    |
        |      |                             |              |                |
        |    +-v------------+                |              +----------------+
        |    |              |                |
        |    |    Search    |                |
        |    |              |                |        +-------------------+
        |    +-^---^--------+                +-------->                   |
        |      |   |                                  |    Delete Book    |
        |      |   +---------------------------------->                   |
        |      |                                      +-------------------+
        |      |       +-------------------+
        |      |       |                   |
        |      +------->    Update Book    |
        +-------------->                   |
                       +-------------------+

    */

    return menu(store, path);
}
