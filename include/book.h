#ifndef _BOOK_H
#define _BOOK_H

/*
 * book.h
 * Author: M00697094
 * Brief: Declares the `Book` class which is used throughout the program as our
 * primary method of storing objects.
 * Created: 26/3/2021
 * Updated: 4/4/2021
 */

#include <string>

/**
 * @brief The Book class holds the properties that the predefined books file
 * has required.
 */
class Book {
   public:
    std::string title;
    std::string authors;
    std::string ISBN;
    int quantity;

    Book();
    Book(const std::string&, const std::string&, const std::string&, int);

    std::string to_tsv();
};

#endif
