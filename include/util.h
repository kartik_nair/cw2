#ifndef _UTIL_H
#define _UTIL_H

/*
 * util.h
 * Author: M00697094
 *
 * Brief: Includes utility functions that have been used throughout the program.
 * This file has been mostly inspired from the work that I had done in CW1 as it
 * provides an independently re-usable library of functions.
 *
 * Created: 14/1/2021
 * Updated: 10/4/2021
 */

#include <cstring>
#include <functional>
#include <iostream>
#include <optional>
#include <sstream>
#include <string>

void clear();
std::fstream open_file(const std::string&);
// note how it takes std::string by value. this is to actually create a copy.
std::string lowercase(std::string);
std::optional<size_t> kmp_search(const std::string&, const std::string&);

template <typename T>
void write(std::ostream& os, const T& value) {
    os << value;
}

template <typename T, typename... Ts>
void write(std::ostream& os, const char* format, const T& value,
           const Ts&... args) {
    size_t i = 0;
    size_t format_len = std::strlen(format);

    while (i < format_len) {
        if (format[i] == '{' && i + 1 < format_len && format[i + 1] == '}') {
            os << value;
            write(os, &format[i + 2], args...);
            return;
        }
        std::cout << format[i];
        i++;
    }
}

template <typename T>
void print(const T& value) {
    write(std::cout, value);
}

template <typename T, typename... Ts>
void print(const char* format, const T& first, const Ts&... rest) {
    write(std::cout, format, first, rest...);
}

template <typename T>
void println(const T& value) {
    write(std::cout, value);
    std::cout << std::endl;
}

template <typename... Ts>
void println(const char* format, const Ts&... rest) {
    write(std::cout, format, rest...);
    std::cout << std::endl;
}

template <typename... Ts>
std::string format(const char* format, const Ts&... rest) {
    std::ostringstream buffer;
    write(buffer, format, rest...);
    return buffer.str();
}

template <typename T>
inline T prompt(
    const std::string& prompt_string,
    std::function<bool(T)> validate = [](T) { return true; },
    const std::string& invalid_informer =
        "That input is invalid. Please try again.\n",
    bool reprint_prompt = true) {
    T value;
    std::string str_value;
    std::cout << prompt_string;
    std::getline(std::cin, str_value);
    std::stringstream value_stream(str_value);

    while (!(value_stream >> value && (value_stream >> std::ws).eof() &&
             validate(value))) {
        std::cout << invalid_informer;
        if (reprint_prompt) std::cout << prompt_string;
        std::getline(std::cin, str_value);
        value_stream.str(str_value);
        value_stream.clear();
    }

    return value;
}

template <>
inline std::string prompt(const std::string& prompt_string,
                          std::function<bool(std::string)> validate,
                          const std::string& invalid_informer,
                          bool reprint_prompt) {
    std::string value;
    std::cout << prompt_string;

    while (std::getline(std::cin, value) && !validate(value)) {
        std::cout << invalid_informer;
        if (reprint_prompt) std::cout << prompt_string;
    }

    return value;
}

#endif
