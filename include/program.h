#ifndef _PROGRAM_H
#define _PROGRAM_H

/*
 * program.h
 * Author: M00697094
 *
 * Brief: Declares the user-facing program functions (to allow for testing
 * apart from main.cpp).
 *
 * Created: 10/4/2021
 * Updated: 10/4/2021
 */

#include <fstream>
#include <optional>
#include <string>

#include "book.h"
#include "map.h"

void parse_tsv(std::istream&, map<std::string, Book>&);
void save_books_to_file(map<std::string, Book>&, const std::string&);
void add_book(map<std::string, Book>&, const std::string&);
std::optional<Book> find_book(map<std::string, Book>&);
void delete_book(map<std::string, Book>&, const std::string&);
void update_book(map<std::string, Book>&, const std::string&);
int menu(map<std::string, Book>&, const std::string&);

#endif
