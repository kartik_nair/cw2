/*
 * test_map.cpp
 * Author: M00697094
 * Brief: Test cases for the hashmap implementation in `include/map.h`.
 * Created: 10/4/2021
 * Updated: 10/4/2021
 */

#include "./catch.hpp"
#include "map.h"

TEST_CASE("Newly created `map` is empty.", "[map::map()]") {
    map<int, int> empty_map;
    REQUIRE(empty_map.empty());
}

TEST_CASE("Adding 1 value makes `.empty()` false.", "[map::empty()]") {
    map<int, int> empty_map;
    empty_map.insert(0, 1);
    REQUIRE(!empty_map.empty());
}

TEST_CASE("Primitive types can be used as keys.", "[map::map()]") {
    // using lambdas is a bit slower but allows us to check for declarations
    REQUIRE_NOTHROW([]() { auto test_map = map<int, int>(); }());
    REQUIRE_NOTHROW([]() { auto test_map = map<unsigned int, int>(); }());
    REQUIRE_NOTHROW([]() { auto test_map = map<long, int>(); }());
    REQUIRE_NOTHROW([]() { auto test_map = map<size_t, int>(); }());
    REQUIRE_NOTHROW([]() { auto test_map = map<float, int>(); }());
    REQUIRE_NOTHROW([]() { auto test_map = map<double, int>(); }());
    REQUIRE_NOTHROW([]() { auto test_map = map<std::string, int>(); }());
}

TEST_CASE("Primitive types can be used as values.", "[map::map()]") {
    REQUIRE_NOTHROW([]() { auto test_map = map<int, int>(); }());
    REQUIRE_NOTHROW([]() { auto test_map = map<int, unsigned int>(); }());
    REQUIRE_NOTHROW([]() { auto test_map = map<int, long>(); }());
    REQUIRE_NOTHROW([]() { auto test_map = map<int, size_t>(); }());
    REQUIRE_NOTHROW([]() { auto test_map = map<int, float>(); }());
    REQUIRE_NOTHROW([]() { auto test_map = map<int, double>(); }());
    REQUIRE_NOTHROW([]() { auto test_map = map<int, std::string>(); }());
}

TEST_CASE(
    "Larger than initial size casues a successful resize (by factor of 2).",
    "[map::insert()]") {
    map<size_t, size_t> test_map;
    size_t initial_capacity = test_map.get_capacity();
    for (size_t i = 0; i < initial_capacity + 2; i++) {
        test_map.insert(i, i);
    }
    REQUIRE(test_map.get_capacity() == (initial_capacity * 2));
}

TEST_CASE(
    "Lookup for existing key is successful (non-existent key returns "
    "std::nulopt).",
    "[map::lookup()]") {
    map<std::string, int> test_map;
    test_map.insert("foo", 4);
    test_map.insert("bar", 7);
    REQUIRE(test_map.lookup("foo").has_value());
    REQUIRE(test_map.lookup("bar").has_value());
    REQUIRE(!test_map.lookup("non-existent").has_value());
}

TEST_CASE("Get returns value for key (& throws for non-existent keys).",
          "[map::get()]") {
    map<std::string, int> test_map;
    test_map.insert("foo", 4);
    test_map.insert("bar", 7);
    REQUIRE(test_map.get("foo") == 4);
    REQUIRE(test_map.get("bar") == 7);
    REQUIRE_THROWS(test_map.get("non-existent"));
}

TEST_CASE("Removal changes results in all methods.", "[map::remove()]") {
    map<std::string, int> test_map;
    REQUIRE(test_map.empty());
    test_map.insert("foo", 4);
    REQUIRE(!test_map.empty());
    REQUIRE(test_map.lookup("foo").has_value());
    test_map.remove("foo");
    REQUIRE(test_map.empty());
    REQUIRE(!test_map.lookup("foo").has_value());
    REQUIRE_THROWS(test_map.get("foo"));
}

TEST_CASE("Inserting on the same key twice overwrites.", "[map::insert()]") {
    map<std::string, int> test_map;
    test_map.insert("foo", 4);
    REQUIRE(test_map.get("foo") == 4);
    test_map.insert("foo", 7);
    REQUIRE(test_map.get("foo") == 7);
}

TEST_CASE("Clearing the map removes all elements.", "[map::insert()]") {
    map<std::string, int> test_map;
    test_map.insert("foo", 4);
    test_map.insert("bar", 7);
    test_map.insert("foobar", 47);
    REQUIRE(test_map.size() == 3);

    test_map.clear();

    REQUIRE(test_map.empty());
    REQUIRE(!test_map.lookup("foo").has_value());
    REQUIRE(!test_map.lookup("bar").has_value());
    REQUIRE(!test_map.lookup("foobar").has_value());
    REQUIRE_THROWS(test_map.get("foo"));
    REQUIRE_THROWS(test_map.get("bar"));
    REQUIRE_THROWS(test_map.get("foobar"));
}