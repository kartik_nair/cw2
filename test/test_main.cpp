/*
 * test_main.cpp
 * Author: M00697094
 * Brief: This file mainly to improve compile speed. See:
 * https://github.com/catchorg/Catch2/blob/v2.x/docs/slow-compiles.md.
 * Created: 14/1/2021
 * Last Updated: 15/1/2021
 */

#define CATCH_CONFIG_MAIN
#include "./catch.hpp"