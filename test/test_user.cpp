/*
 * test_user.cpp
 * Author: M00697094
 *
 * Brief: Test cases for the user-facing functions declared in
 * `include/program.h`. Mainly testing for input validation and
 * expected output.
 *
 * Note that some file operations require a bash-like shell (so either zsh,
 * bash, cygwin, gitbash, wsl, etc.)
 *
 * Created: 10/4/2021
 * Updated: 10/4/2021
 */

#include <iostream>
#include <sstream>
#include <vector>

#include "./catch.hpp"
#include "book.h"
#include "map.h"
#include "program.h"
#include "util.h"

static std::streambuf* original_cin = std::cin.rdbuf();
static std::streambuf* original_cout = std::cout.rdbuf();
static std::istringstream test_input;
static std::ostringstream test_output;
static map<std::string, Book> test_store;

TEST_CASE("Parsing valid TSV behaves as expected.", "[parse_tsv()]") {
    std::string test_string =
        "test title\ttest author 1; test author 2\t971000000\t45\t\n";
    std::istringstream test_stream(test_string);
    parse_tsv(test_stream, test_store);
    REQUIRE(test_store.size() == 1);
    REQUIRE_NOTHROW(test_store.get("test title"));
    REQUIRE(test_store.get("test title").authors ==
            "test author 1; test author 2");
    REQUIRE(test_store.get("test title").ISBN == "971000000");
    REQUIRE(test_store.get("test title").quantity == 45);

    test_store.clear();
}

TEST_CASE("Parsing invalid TSV throws an error.", "[parse_tsv()]") {
    std::string only_three_columns =
        "test titletest author 1; test author 2\t971000000\t45\t\n";
    std::string five_columns =
        "test title\ttest author 1; test author 2\t971000000\t45\ttest "
        "more\t\n";
    std::string non_numeric_quantity =
        "test title\ttest author 1; test author 2\t971000000\tnon numeric\t\n";

    std::istringstream only_three_columns_stream(only_three_columns);
    std::istringstream five_columns_stream(five_columns);
    std::istringstream non_numeric_quantity_stream(non_numeric_quantity);

    REQUIRE_THROWS(parse_tsv(only_three_columns_stream, test_store));
    REQUIRE_THROWS(parse_tsv(five_columns_stream, test_store));
    REQUIRE_THROWS(parse_tsv(non_numeric_quantity_stream, test_store));

    test_store.clear();
}

TEST_CASE("Saving store to file writes valid TSV.", "[save_books_to_file()]") {
    system("touch .test_for_write.txt");

    Book test_book_1 = Book("test book 1", "test authors", "971000000", 1);
    Book test_book_2 = Book("test book 2", "test authors", "971000000", 2);
    Book test_book_3 = Book("test book 3", "test authors", "971000000", 1);

    test_store.insert(test_book_1.title, test_book_1);
    test_store.insert(test_book_2.title, test_book_2);
    test_store.insert(test_book_3.title, test_book_3);

    save_books_to_file(test_store, ".test_for_write.txt");

    // since parsing has been tested _before_ writing we can use our
    // parse_tsv function to check if the written contents are valid
    std::fstream test_file = open_file(".test_for_write.txt");
    REQUIRE_NOTHROW(parse_tsv(test_file, test_store));
    test_file.close();

    test_store.clear();
    system("rm -rf .test_for_write.txt");
}

TEST_CASE("Add book validates user-input and adds new book to store.",
          "[add_book()]") {
    system("touch .test_temp.txt");

    test_input.clear();
    test_input.str(
        "test title\ntest authors\ninvalid isbn"
        "\n971000000\ninvalid qty\n5\n");
    test_output.clear();
    test_output.str("");

    std::cin.rdbuf(test_input.rdbuf());
    std::cout.rdbuf(test_output.rdbuf());

    REQUIRE_NOTHROW(add_book(test_store, ".test_temp.txt"));
    REQUIRE(test_store.contains("test title"));

    std::cin.rdbuf(original_cin);
    std::cout.rdbuf(original_cout);

    test_store.clear();
    system("rm -rf .test_temp.txt");
}

TEST_CASE(
    "Find book allows user to do partial string-searching & exact title lookup "
    "successfully.",
    "[find_book()]") {
    system("touch .test_temp.txt");

    test_output.clear();
    test_input.clear();
    test_output.str("");
    std::cout.rdbuf(test_output.rdbuf());

    Book test_book_1 = Book("test book 1", "test authors", "971000000", 1);
    Book test_book_2 = Book("test book 2", "test authors", "971000000", 2);
    Book test_book_3 = Book("test book 3", "test authors", "971000000", 1);

    test_store.insert(test_book_1.title, test_book_1);
    test_store.insert(test_book_2.title, test_book_2);
    test_store.insert(test_book_3.title, test_book_3);

    // exact title lookup
    test_input.str("1\ntest book 1\n");
    std::cin.rdbuf(test_input.rdbuf());

    REQUIRE(find_book(test_store).has_value());

    test_input.str("1\ntest non existent\n");
    std::cin.rdbuf(test_input.rdbuf());

    REQUIRE(!find_book(test_store).has_value());

    // partial searching
    test_input.str("2\ntest\n5\n1");
    REQUIRE(find_book(test_store).has_value());

    std::cin.rdbuf(original_cin);
    std::cout.rdbuf(original_cout);

    test_store.clear();
    system("rm -rf .test_temp.txt");
}

TEST_CASE("Delete book validates user-input and deletes found book from store.",
          "[delete_book()]") {
    system("touch .test_temp.txt");

    Book test_book_1 = Book("test book 1", "test authors", "971000000", 1);
    test_store.insert(test_book_1.title, test_book_1);

    test_input.clear();
    test_input.str("1\ntest non existent\n1\ntest book 1\n");
    test_output.clear();
    test_output.str("");

    std::cin.rdbuf(test_input.rdbuf());
    std::cout.rdbuf(test_output.rdbuf());

    REQUIRE_NOTHROW(delete_book(test_store, ".test_temp.txt"));
    REQUIRE(!test_store.contains("test book 1"));

    std::cin.rdbuf(original_cin);
    std::cout.rdbuf(original_cout);

    test_store.clear();
    system("rm -rf .test_temp.txt");
}

TEST_CASE("Update book validates user-input and updates found book from store.",
          "[update_book()]") {
    system("touch .test_temp.txt");

    Book test_book_1 = Book("test book 1", "test authors", "971000000", 1);
    test_store.insert(test_book_1.title, test_book_1);

    test_input.clear();
    test_input.str(
        "1\ntest non existent\n1\ntest book 1\n500\n4\nstring\n56\n");
    test_output.clear();
    test_output.str("");

    std::cin.rdbuf(test_input.rdbuf());
    std::cout.rdbuf(test_output.rdbuf());

    REQUIRE_NOTHROW(update_book(test_store, ".test_temp.txt"));
    REQUIRE(test_store.contains("test book 1"));
    INFO(test_output.str());
    REQUIRE(test_store.get("test book 1").quantity == 56);

    std::cin.rdbuf(original_cin);
    std::cout.rdbuf(original_cout);

    test_store.clear();
    system("rm -rf .test_temp.txt");
}

TEST_CASE("Main menu validates user-input and allows navigation/exit.",
          "[menu()]") {
    system("touch .test_temp.txt");

    test_input.clear();
    test_input.str("string\n500\n0\n");
    test_output.clear();
    test_output.str("");

    std::cin.rdbuf(test_input.rdbuf());
    std::cout.rdbuf(test_output.rdbuf());

    REQUIRE(menu(test_store, ".test_temp.txt") == 0);

    std::cin.rdbuf(original_cin);
    std::cout.rdbuf(original_cout);

    system("rm -rf .test_temp.txt");
}
