/*
 * test_book.cpp
 * Author: M00697094
 * Brief: Test cases for the Book class.
 * Created: 12/4/2021
 * Updated: 12/4/2021
 */

#include "./catch.hpp"
#include "book.h"

TEST_CASE("Is default constructible.", "[Book::Book()]") {
    /*
        Note that creating a pointer like below doesn't call the default
        constructor. A simple test to see this is available at:
        https://replit.com/@KartikNair/default-constructor.

        This can be confusing because declaring a value the same way (like
        `Book test_book;`) would in fact call the default contructor. But the
        default constructor for any pointer is just `nullptr`.
    */
    Book* test_book;
    REQUIRE_NOTHROW(test_book = new Book());
    delete test_book;
}

TEST_CASE("Can be constructed with values (and values are readable).",
          "[Book::Book()]") {
    Book* test_book;
    REQUIRE_NOTHROW(test_book =
                        new Book("test book", "test authors", "971000000", 1));
    REQUIRE(test_book->title == "test book");
    REQUIRE(test_book->authors == "test authors");
    REQUIRE(test_book->ISBN == "971000000");
    REQUIRE(test_book->quantity == 1);
    delete test_book;
}

TEST_CASE("Books are accurately transformed to TSV.", "[Book::to_tsv()]") {
    Book test_book("test book", "test authors", "971000000", 1);
    REQUIRE(test_book.to_tsv() == "test book\ttest authors\t971000000\t1\t\n");
}